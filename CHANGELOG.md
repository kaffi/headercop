History
=======

0.1.0 (2017-04-22)
------------------

* First release.

0.1.1 (2017-05-16)
------------------

* Include licenses in package (bsd3 for now)

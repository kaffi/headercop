headercop
=========

[![build status](https://gitlab.com/kaffi/headercop/badges/master/build.svg)](https://gitlab.com/kaffi/headercop/commits/master)
[![coverage report](https://gitlab.com/kaffi/headercop/badges/master/coverage.svg)](https://gitlab.com/kaffi/headercop/commits/master)

Enforce license headers in files.


Features
--------

* Check to see if file headers are present in files. Useful for CI pipelines.
* Inject file headers if missing to files
* Built in support for Python and Terraform files, easy to add support for others.


TODO
----

* Proper documentation...
* Remove headers (in prepareation to change headers, e.g. copyright year)


Credits
-------

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[audreyr/cookiecutter-pypackage](https://github.com/audreyr/cookiecutter-pypackage)
project template.

